# jimbos_challenge

#Use Case:
Design, develop, and implement a Jenkins build server that makes use of persistent storage. The build server must be built within a secure subnet.

##Requirements:
- CSP of your choosing
- IaC for infrastructure build
- Configuration Management for application configuration
- Monitoring
- Cost effective solution
- Security Considerations
- Documentation

More information about Jenkins: https://jenkins.io/ 

##Extra Features
Add slave nodes to the Jenkins deployment

#Team Expectation:
Design a solution for the above use case. Build a backlog of all the tasks requirement to implement the designed solution. And proceed to develop, test, and deploy the solution. Adequate documentation must also be prepared as part of the closure of the implemented solution. 